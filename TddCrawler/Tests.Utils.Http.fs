﻿module Tests.Utils.Http

open FsUnit
open NUnit.Framework
open Utils.Http

let private syncFail = (Some(System.TimeSpan.FromMilliseconds 10.0) |> getHtmlWithTimeout) >> Async.RunSynchronously
let private sync = getHtml >> Async.RunSynchronously

[<Test>]
//[<Explicit>]
let ``getHtml Immutable_Object``() = 
    let (Ok content) = sync "Immutable_object"
    content.Length |> should greaterThan 100

[<Test>]
//[<Explicit>]
let ``getHtml error``() = 
    syncFail "Immutable_object" |> should equal Timeout
    

[<Test>]
//[<Explicit>]
let ``getHtml 404 error``() = 
    let (HttpError code) = sync "Immutable-Object"
    code |> should equal 404
    

