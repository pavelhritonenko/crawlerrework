﻿module Utils.Http

open System
open System.Net
open System.Net.Http

ServicePointManager.UseNagleAlgorithm <- true
ServicePointManager.DefaultConnectionLimit <- 1000

let private client (timeout: TimeSpan option) = 
    let c = new HttpClient()
    c.BaseAddress <- new Uri("http://en.wikipedia.org/wiki/")
    match timeout with
    | Some t -> c.Timeout <- t
    | None -> ()
    c

type HtmlResult = 
    | Ok of string
    | HttpError of int
    | Error of Exception
    | Timeout

type Async = 
    static member StartCatchCancellation(work, ?cancellationToken) = 
        Async.FromContinuations(fun (cont, econt, _) -> 
            let ccont e = econt e
            Async.StartWithContinuations(work, cont, econt, ccont, ?cancellationToken = cancellationToken))

let private await t = 
    t
    |> Async.AwaitTask
    |> Async.StartCatchCancellation

let private getHtmlWithTimeoutImpl (c : HttpClient) (pageId : string) = 
    async {
        try
            use! response = c.GetAsync(pageId) |> await

            if not response.IsSuccessStatusCode then
                return (HttpError (int response.StatusCode))
            else
                use content = response.EnsureSuccessStatusCode().Content
                let! html = content.ReadAsStringAsync() |> await
                return Ok html
        with
            | :? OperationCanceledException -> return Timeout
            | e -> return Error e
    }

let getHtmlWithTimeout timeout =
    client timeout |>
    getHtmlWithTimeoutImpl


let getHtml = client None |> getHtmlWithTimeoutImpl
