﻿module Tests.Parse

open NUnit.Framework
open FsUnit
open System.IO
open Parse

let immutableObjectHtml = 
    let assembly = System.Reflection.Assembly.GetExecutingAssembly()
    let stream = assembly.GetManifestResourceStream("Immutable_object.html")
    use reader = new StreamReader(stream)
    reader.ReadToEnd()

let private (==) actual expected = actual |> should equal expected

[<Test>]
let ``immutable object title`` () =
    title immutableObjectHtml == Some "Immutable object"

[<Test>]
let ``references count`` () =
    (references immutableObjectHtml).Length == 62

[<Test>]
let ``reference names should not contain colon`` () =
    references immutableObjectHtml |> List.filter (fun x -> x.Contains(":")) |> should be Empty