﻿// Learn more about F# at http://fsharp.net
// See the 'F# Tutorial' project for more help.
[<EntryPoint>]
let main argv = 
    let c = Crawler.create()
    
    let startCrawling() = 
        async { 
            let! result = Crawler.start c { Id = Crawler.ArticleId "Immutable_object"
                                            Depth = 3 }
            printf "%A" result
        }
    
    let rec diagnostic() = 
        async { 
            do! Async.Sleep 1000

            let queue = c.CurrentQueueLength

            if queue < 100 then

                let! stat = c.PostAndAsyncReply(fun c -> Crawler.Status(c))
                printfn "%d done, %d pending, %d queue length, %d retries, %d errors"
                    stat.Articles.Count stat.Pending queue stat.Retries stat.Errors.Length
                if stat.Pending <> 0 then return! diagnostic()
            else
                printfn "queue length %d" queue
                return! diagnostic()
        }
    
    startCrawling() |> Async.Start
    diagnostic() |> Async.Start
    printfn "press [ENTER] for exit..."
    System.Console.ReadLine() |> ignore
    0
