﻿module Crawler

type ArticleId = 
    | ArticleId of string

type Target = 
    { Id : ArticleId
      Depth : int }

type Article = 
    { id : ArticleId
      Title : string option
      References : Set<ArticleId> }

type ExternalState = 
    { Articles : Set<Article>
      Errors : string list
      Pending : int
      Retries : int }

type Message = 
    | Start of Target * AsyncReplyChannel<ExternalState>
    | Fetch of Target
    | Append of Article * depth : int
    | Error of Target * string
    | Status of AsyncReplyChannel<ExternalState>
    | Retry of Target

type State = 
    { AsyncDownloads : int
      Errors : string list
      Ids : Set<ArticleId>
      Result : Set<Article>
      Reply : AsyncReplyChannel<ExternalState> option
      Retries: int }

let export state = 
    { Articles = state.Result
      Errors = state.Errors
      Pending = state.AsyncDownloads
      Retries = state.Retries }

let parse id html = 
    { id = id
      Title = Parse.title html
      References = new Set<ArticleId>(Parse.references html |> List.map (fun x -> ArticleId x)) }

type agent = MailboxProcessor<Message>

let get = Utils.Http.getHtmlWithTimeout (Some <| System.TimeSpan.FromMinutes 30.0)

let processMessage (inbox : agent) msg state = 
    let fetchAndParse (target : Target) = 
        async { 
            let (ArticleId url) = target.Id
            let! page = get url
            match page with
            | Utils.Http.HtmlResult.Error _ ->  Retry target |> inbox.Post
            | Utils.Http.HtmlResult.Ok content -> 
                let article = parse target.Id content
                Append(article, target.Depth - 1) |> inbox.Post
            | Utils.Http.HttpError code -> Error(target, sprintf "HTTP-CODE %d" code) |> inbox.Post
            | Utils.Http.Timeout -> Retry target |> inbox.Post
        }
    match msg with
    | Start(target, reply) -> 
        inbox.Post <| Fetch target
        { state with Reply = Some reply }
    | Fetch target -> 
        fetchAndParse target |> Async.Start
        { state with AsyncDownloads = state.AsyncDownloads + 1 }
    | Retry target -> 
        fetchAndParse target |> Async.Start
        { state with Retries = state.Retries + 1 }
    | Append(article, depth) -> 
        if depth > 0 then 
            let newArticles = article.References - state.Ids
            for id in newArticles do
                Fetch { Id = id
                        Depth = depth }
                |> inbox.Post
            { state with AsyncDownloads = state.AsyncDownloads - 1
                         Result = state.Result.Add article
                         Ids = state.Ids + article.References }
        else 
            { state with AsyncDownloads = state.AsyncDownloads - 1
                         Result = state.Result.Add article }
    | Error(_, message) -> 
        { state with AsyncDownloads = state.AsyncDownloads - 1
                     Errors = message :: state.Errors }
    | Status reply -> 
        reply.Reply <| export state
        state

let create() = 
    agent.Start(fun inbox -> 
        let rec loop (s : State) = 
            async { 
                let! msg = inbox.Receive()
                let newState = processMessage inbox msg s
                if newState.AsyncDownloads = 0 && inbox.CurrentQueueLength = 0 then 
                    match s.Reply with
                    | None -> ()
                    | Some c -> export newState |> c.Reply
                return! loop newState
            }
        loop { AsyncDownloads = 0
               Errors = []
               Ids = Set.empty<ArticleId>
               Result = Set.empty<Article>
               Reply = None
               Retries = 0 })

let start (agent : agent) target = async { return! agent.PostAndAsyncReply(fun c -> (Start(target, c))) }
