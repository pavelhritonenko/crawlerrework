﻿module Parse

open System
open System.Text.RegularExpressions

let title html =
    let m = Regex.Match(html, """\<span dir=\"auto\"\>(.*)\<\/span\>""")
    match m.Success with
    | false -> None
    | true -> Some(m.Groups.Item(1).Value)

let references html =
    Regex.Matches(html, """"/wiki/([^:]*?)[\"\#]""")
    |> Seq.cast<Match>
    |> Seq.map (fun x -> x.Groups.Item(1).Value)
    |> Seq.distinct
    |> List.ofSeq

